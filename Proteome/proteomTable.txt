#Bilateria

#Abkuerzung	Spezies	Source	Proteom	Genom	Paper

DMEL	Drosophila melanogaster	NCBI	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/215/GCF_000001215.4_Release_6_plus_ISO1_MT/GCF_000001215.4_Release_6_plus_ISO1_MT_protein.faa.gz	https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/215/GCF_000001215.4_Release_6_plus_ISO1_MT/GCF_000001215.4_Release_6_plus_ISO1_MT_genomic.fna.gz	---

HMIA	Hofstenia miamia	EnsemblMetazoa	ftp://ftp.ensemblgenomes.org/pub/metazoa/release-47/fasta/hofstenia_miamia/pep/Hofstenia_miamia.HmiaM1.pep.all.fa.gz	ftp.ensemblgenomes.org/pub/metazoa/release-47/fasta/hofstenia_miamia/dna/Hofstenia_miamia.HmiaM1.dna.toplevel.fa.gz	---


#Cnidaria

#Abkuerzung	Spezies	Source	Proteom	Genom	Paper

NVEC	Nematostella vectensis	NCBI	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/209/225/GCF_000209225.1_ASM20922v1/GCF_000209225.1_ASM20922v1_protein.faa.gz	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/209/225/GCF_000209225.1_ASM20922v1/GCF_000209225.1_ASM20922v1_genomic.fna.gz	---

TKIT	Thelohanellus kitauei	NCBI	ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/827/895/GCA_000827895.1_ASM82789v1/GCA_000827895.1_ASM82789v1_protein.faa.gz	ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/827/895/GCA_000827895.1_ASM82789v1/GCA_000827895.1_ASM82789v1_genomic.fna.gz	---

EPAL	Exaiptasia pallida	NCBI	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/001/417/965/GCF_001417965.1_Aiptasia_genome_1.1/GCF_001417965.1_Aiptasia_genome_1.1_protein.faa.gz	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/001/417/965/GCF_001417965.1_Aiptasia_genome_1.1/GCF_001417965.1_Aiptasia_genome_1.1_genomic.fna.gz	---

HVUL	Hydra vulgaris	NCBI	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/004/095/GCF_000004095.1_Hydra_RP_1.0/GCF_000004095.1_Hydra_RP_1.0_protein.faa.gz	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/004/095/GCF_000004095.1_Hydra_RP_1.0/GCF_000004095.1_Hydra_RP_1.0_genomic.fna.gz	"Conservation of the nucleotide excision repair pathway: characterization of hydra Xeroderma Pigmentosum group F homolog". Barve A, et al. 2013

ATEN	Actinia tenebrosa	NCBI	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/009/602/425/GCF_009602425.1_ASM960242v1/GCF_009602425.1_ASM960242v1_protein.faa.gz	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/009/602/425/GCF_009602425.1_ASM960242v1/GCF_009602425.1_ASM960242v1_genomic.fna.gz	---

AMIL	Acropora millepora	NCBI	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/004/143/615/GCF_004143615.1_amil_sf_1.1/GCF_004143615.1_amil_sf_1.1_protein.faa.gz	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/004/143/615/GCF_004143615.1_amil_sf_1.1/GCF_004143615.1_amil_sf_1.1_genomic.fna.gz	---

PDAM	Pocillopora damicornis	NCBI	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/003/704/095/GCF_003704095.1_ASM370409v1/GCF_003704095.1_ASM370409v1_protein.faa.gz	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/003/704/095/GCF_003704095.1_ASM370409v1/GCF_003704095.1_ASM370409v1_genomic.fna.gz	---

SPIS	Stylophora pistillata	NCBI	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/002/571/385/GCF_002571385.1_Stylophora_pistillata_v1/GCF_002571385.1_Stylophora_pistillata_v1_protein.faa.gz	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/002/571/385/GCF_002571385.1_Stylophora_pistillata_v1/GCF_002571385.1_Stylophora_pistillata_v1_genomic.fna.gz	---

OFAV	Orbicella faveolata	NCBI	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/002/042/975/GCF_002042975.1_ofav_dov_v1/GCF_002042975.1_ofav_dov_v1_protein.faa.gz	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/002/042/975/GCF_002042975.1_ofav_dov_v1/GCF_002042975.1_ofav_dov_v1_genomic.fna.gz	---

DGIG	Dendronephthya gigantea	NCBI	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/004/324/835/GCF_004324835.1_DenGig_1.0/GCF_004324835.1_DenGig_1.0_protein.faa.gz	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/004/324/835/GCF_004324835.1_DenGig_1.0/GCF_004324835.1_DenGig_1.0_genomic.fna.gz	---

MSQU	Myxobolus squamalis	NCBI	ftp.ncbi.nlm.nih.gov/genomes/all/GCA/010/108/815/GCA_010108815.1_TAU_Msqu_1/GCA_010108815.1_TAU_Msqu_1_protein.faa.gz	ftp.ncbi.nlm.nih.gov/genomes/all/GCA/010/108/815/GCA_010108815.1_TAU_Msqu_1/GCA_010108815.1_TAU_Msqu_1_genomic.fna.gz	---

#Placozoa

#Abkuerzung	Spezies	Source	Proteom	Genom	Paper

TADH	Trichoplax adherens	NCBI	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/150/275/GCF_000150275.1_v1.0/GCF_000150275.1_v1.0_protein.faa.gz	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/150/275/GCF_000150275.1_v1.0/GCF_000150275.1_v1.0_genomic.fna.gz	Bernd Schierwater (2009) "Placozoa and the Evolution of Metazoa and Intrasomatic Cell Differentiation"

TSPH	Trichoplax sp. H2	NCBI	https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/003/344/405/GCA_003344405.1_TrispH2_1.0/GCA_003344405.1_TrispH2_1.0_protein.faa.gz	---	Kamm K, et al. (2018) "Trichoplax genomes reveal profound admixture and suggest stable wild populations without bisexual reproduction. 

HHON	Hoilungia hongkongensis	bitbucket	---	https://bitbucket.org/molpalmuc/hoilungia-genome/src/master/sequences/Hhon_final_contigs_unmasked.fasta.gz	Eitel, M. et al. (2018) "Comparative genomics and the nature of placozoan species." PLoS Biology 16(7) : e2005359.

#Porifera

#Abkuerzung	Spezies	Source	Proteom	Genom	Paper

AQUE	Amphimedon queenslandica	NCBI	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/090/795/GCF_000090795.1_v1.0/GCF_000090795.1_v1.0_protein.faa.gz	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/090/795/GCF_000090795.1_v1.0/GCF_000090795.1_v1.0_genomic.fna.gz	Srivastava M, et al. (2010) "The Amphimedon queenslandica genome and the evolution of animal complexity"

SCAR	Stylissa carteri	Comapgen	www.compagen.org/datasets/SCAR_T-PEP_160304.zip	---	Ryu et al. (2016) "Hologenome analysis of two marine sponges with different microbiomes"

XTES	Xestospongia testudinaria	Compagen	www.compagen.org/datasets/XTES_T-PEP_160304.zip	---	Ryu et al. (2016) "Hologenome analysis of two marine sponges with different microbiomes"

OCAR	Oscarella carmela	Compagen	www.compagen.org/datasets/OCAR_T-PEP_130911.zip	www.compagen.org/datasets/OCAR_WGA_120614.zip	---

EMUE	Ephydatia muelleri	Compagen	www.compagen.org/datasets/EMUE_T-PEP_130911.zip	---	---

SCIL	Sycon ciliatum	Compagen	www.compagen.org/datasets/SCIL_T-PEP_130802.zip	www.compagen.org/datasets/SCIL_WGA_130802.zip	Oliver Voigt et al. (2012) "Molecular Phylogenetic Evaluation of Classification and Scenarios of Character Evolution in Calcareous Sponges (Porifera, Class Calcarea)"


#Ctenophora

#Abkuerzung	Spezies	Source	Proteom	Genom	Paper

MLEI	Mnemiopsis leidyi	EnsemblGenomes	ftp.ensemblgenomes.org/pub/release-47/metazoa/fasta/mnemiopsis_leidyi/pep/Mnemiopsis_leidyi.MneLei_Aug2011.pep.all.fa.gz	ftp.ensemblgenomes.org/pub/release-47/metazoa/fasta/mnemiopsis_leidyi/dna/Mnemiopsis_leidyi.MneLei_Aug2011.dna.toplevel.fa.gz	---

PBAC	Pleurobrachia bachei	NCBI	---	ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/695/325/GCA_000695325.1_P.bachei_draft_genome_v.1.1/GCA_000695325.1_P.bachei_draft_genome_v.1.1_genomic.fna.gz	Leonid L. Moroz et al. (2014) "The ctenophore genome and the evolutionary origins of neural systems" (first part)

BFOR	Beroe forskalii	NCBI	---	https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/011/033/025/GCA_011033025.1_UCSC_Bfor_v1/GCA_011033025.1_UCSC_Bfor_v1_genomic.fna.gz	---

BOVA	Beroe ovata	NCBI	---	https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/900/239/995/GCA_900239995.1_ASM90023999v1/GCA_900239995.1_ASM90023999v1_genomic.fna.gz	---


#Choanoflagellata

#ANMERKUNG: 19 der hier genannten Spezies haben offensichtlich den selben Downloadlink (das liegt daran, dass sie alle aus dem Supplement des selben Papers stammen!) - es waere also im Interesse aller, diese Links nur EIN mal herunterzuladen ;)

#Abkuerzung	Spezies	Source	Proteom	Genom	Paper

HGRA	Hartaetosiga gracilis	Richter 2018	ndownloader.figshare.com/files/9955288	---	---

HBAL	Hartaetosiga balthica	Richter 2018	ndownloader.figshare.com/files/9955288	---	---

SINF	Salpingoeca infusionum	Richter 2018	ndownloader.figshare.com/files/9955288	---	---

SROS	Salpinogoeca rosetta	NCBI	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/188/695/GCF_000188695.1_Proterospongia_sp_ATCC50818/GCF_000188695.1_Proterospongia_sp_ATCC50818_protein.faa.gz	---	---

MROA	Microstomoeca roanoka	Richter 2018	ndownloader.figshare.com/files/9955288	---	---

MBRE	Monosiga brevicollis	NCBI	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/002/865/GCF_000002865.3_V1.0/GCF_000002865.3_V1.0_protein.faa.gz	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/002/865/GCF_000002865.3_V1.0/GCF_000002865.3_V1.0_genomic.fna.gz	---

SKVE	Salpinogoeca kvevrii	Richter 2018	ndownloader.figshare.com/files/9955288	---	---

CPER	Choanoeca perplexa	Richter 2018	ndownloader.figshare.com/files/9955288	---	---

MFLU	Mytnosiga fluctuans	Richter 2018	ndownloader.figshare.com/files/9955288	---	---

SURC	Salpinogaeca urceloata	Richter 2018	ndownloader.figshare.com/files/9955288	---	---

SMAC	Salpinogaeca macrocollata	Richter 2018	ndownloader.figshare.com/files/9955288	---	---

SPUN	Salpinogaeca punica	Richter 2018	ndownloader.figshare.com/files/9955288	---	---

SHEL	Salpinogaeca helianthica	NCBI	ndownloader.figshare.com/files/9955288	---	---

CHOL	Codosiga hollandica	Richter 2018	ndownloader.figshare.com/files/9955288	---	---

SDOL	Salpingoeca dolichothecata	Richter 2018	ndownloader.figshare.com/files/9955288	---	---

SPAR	Savillea parva	Richter 2018	ndownloader.figshare.com/files/9955288	---	---

ASPE	Acanthoma spectabilis	Richter 2018	ndownloader.figshare.com/files/9955288	---	---

HNAN	Helgoeca nana	Richter 2018	ndownloader.figshare.com/files/9955288	---	---

DCOS	Didymoeca costata	Richter 2018	ndownloader.figshare.com/files/9955288	---	---

DGRA	Diaphanoeca grandis	Richter 2018	ndownloader.figshare.com/files/9955288	---	---

SDIP	Stephanoeca diplocostata	Richter 2018	ndownloader.figshare.com/files/9955288	---	---


#Filasterea

#Abkuerzung	Spezies	Source	Proteom	Genom	Paper

COWC	Capsaspora owczarzaki	NCBI	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/151/315/GCF_000151315.2_C_owczarzaki_V2/GCF_000151315.2_C_owczarzaki_V2_protein.faa.gz	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/151/315/GCF_000151315.2_C_owczarzaki_V2/GCF_000151315.2_C_owczarzaki_V2_genomic.fna.gz	Hiroshi Suga (2013) "The Capsaspora genome reveals a complex unicellular prehistory of animals"

MVIB	Ministeria vibrans	---	---	---	---


#Teretosporea

#Abkuerzung	Spezies	Source	Proteom	Genom	Paper

CFRA	Creolimax fragrantissim	NCBI	---	ftp.ncbi.nlm.nih.gov/genomes/all/GCA/002/024/145/GCA_002024145.1_C_fragrantissima_v5/GCA_002024145.1_C_fragrantissima_v5_genomic.fna.gz	---

SARC	Sphaeroforma arctica	NCBI	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/001/186/125/GCF_001186125.1_Spha_arctica_JP610_V1/GCF_001186125.1_Spha_arctica_JP610_V1_protein.faa.gz	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/001/186/125/GCF_001186125.1_Spha_arctica_JP610_V1/GCF_001186125.1_Spha_arctica_JP610_V1_genomic.fna.gz	---

IHOF	Ichthyophonus hoferi	NCBI	---	ftp.ncbi.nlm.nih.gov/genomes/all/GCA/002/751/075/GCA_002751075.1_ASM275107v1/GCA_002751075.1_ASM275107v1_genomic.fna.gz	---

APAR	Amoebidium parasiticum	---	---	---	---

AWHI	Abeoforma whisleri	NCBI	---	ftp.ncbi.nlm.nih.gov/genomes/all/GCA/002/812/265/GCA_002812265.1_ASM281226v1/GCA_002812265.1_ASM281226v1_genomic.fna.gz	---

PGEM	Pirum gemmata	NCBI	---	ftp.ncbi.nlm.nih.gov/genomes/all/GCA/002/812/295/GCA_002812295.1_ASM281229v1/GCA_002812295.1_ASM281229v1_genomic.fna.gz	---

CPER	Chromosphaera perkinsii	NCBI	---	ftp.ncbi.nlm.nih.gov/genomes/all/GCA/002/811/675/GCA_002811675.1_ASM281167v1/GCA_002811675.1_ASM281167v1_genomic.fna.gz	---

SDES	Sphaerothecum destruens	---	---	---	---

CLIM	Corallochytrium limacisporum	NCBI	---	https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/002/811/645/GCA_002811645.1_ASM281164v1/GCA_002811645.1_ASM281164v1_genomic.fna.gz	---


# Fungi

#Abkuerzung	Spezies	Source	Proteom	Genom	Paper

SCER	Saccharomyces cerevisiae	NCBI	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/146/045/GCF_000146045.2_R64/GCF_000146045.2_R64_protein.faa.gz	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/146/045/GCF_000146045.2_R64/GCF_000146045.2_R64_genomic.fna.gz	---

NCRA	Neurospora crassa	NCBI	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/182/925/GCF_000182925.2_NC12/GCF_000182925.2_NC12_protein.faa.gz	https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/182/925/GCF_000182925.2_NC12/GCF_000182925.2_NC12_genomic.fna.gz	---


#Viridiplantae

#Abkuerzung	Spezies	Source	Proteom	Genom	Paper

ATHA	Arabidopsis thaliana	NCBI	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/735/GCF_000001735.4_TAIR10.1/GCF_000001735.4_TAIR10.1_protein.faa.gz	ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/735/GCF_000001735.4_TAIR10.1/GCF_000001735.4_TAIR10.1_genomic.fna.gz	---




