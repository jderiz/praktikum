#!/bin/bash

# create esl index for all organisms
for organism in */*.faa
do
	esl-sfetch --index organism
done


