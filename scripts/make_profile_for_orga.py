"""
@author: Jannis de Riz
"""
import os
import pickle

import pandas as pd

import constants

# define Proteins by Domains they contain

with open("hmm_dom_orga_hits.pkl", "rb") as handle:
    orga_hits: {pd.DataFrame} = pickle.load(handle)

orga_domain_scores = {}
raw_domain_scores = {}
by_target = {}

for name, orga in orga_hits.items():
    orga[
        ["dom_score", "acc", "dom_bias", "dom_i_eval", "dom_c_eval", "fs_eval"]
    ] = orga[
        ["dom_score", "acc", "dom_bias", "dom_i_eval", "dom_c_eval", "fs_eval"]
    ].astype(
        float
    )  # make comparable

    orga_no_cutoff = orga
    # set cutoffs from eddylab.org
    orga = orga[
        (orga.dom_i_eval < 1)
        & (orga.dom_c_eval < 1)
        & (orga.dom_score > orga.dom_bias)
        & (orga.fs_eval < 10e-3)
    ]
    orga_domain_profile = orga.groupby("domain")
    dom_by_target = orga.groupby("target")

    raw_domain_scores.update({name: orga_domain_profile})  # no Cutoff
    orga_domain_scores.update({name: orga_domain_profile})
    by_target.update({name: dom_by_target})
# Write results to pickle
with open("orga_domain_profiles.pkl", "wb") as handle:
    pickle.dump(raw_domain_scores, handle)
with open("orga_domain_profiles_cutoff.pkl", "wb") as handle:
    pickle.dump(orga_domain_scores, handle)
with open("orga_dom_by_target.pkl", "wb") as handle:
    pickle.dump(by_target, handle)
