#!/bin/bash 

cd /domainAlignments
echo $PWD
for gene in *.fasta
do
	clustalo -i gene -t Protein -o msa/${gene}_msa.fasta
done

