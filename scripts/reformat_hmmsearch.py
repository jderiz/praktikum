#!/usr/bin/env python3
"""
@author: Jannis de Riz
"""
import os
import pickle

import pandas as pd

import constants as CONST
from Bio import SearchIO

"""
reformat search domain table into DataFrame
"""
CWD = os.getcwd()
# read Domain Hits tables and count
TAXA = CONST.TAXA
DOMAINS = CONST.DOMAINS
attribs = [
    "target",
    "t_accession",
    "tlen",
    "domain",
    "q_accession",
    "qlen",
    "fs_eval",
    "fs_score",
    "fs_bias",
    "#",
    "of",
    "dom_c_eval",
    "dom_i_eval",
    "dom_score",
    "dom_bias",
    "hmfrom",
    "hmto",
    "alifrom",
    "alito",
    "envfrom",
    "envto",
    "acc",
    "descript_target",
]

taxa_dict = {k: {} for k in TAXA}  # each taxa one dict

orga_hits = {}
# TAXA = ["Bilateria"]
# DOMAIN Hits

for taxon in TAXA:
    for organism in os.listdir("hmmsearchOUT/" + taxon):
        print(organism.lower())
        this_hits = []

        if not organism.lower().endswith(
            tuple(["mis", "missing"])
        ) and not organism.startswith(
            "missing"
        ):  # dont use missing

            for domain in DOMAINS:
                if not domain.endswith("DS_Store"):
                    domain = domain[:-4]
                    with open(
                        "hmmsearchOUT/"
                        + taxon
                        + "/"
                        + organism
                        + "/"
                        + domain
                        + "/domHits.dtbl"
                    ) as handle:

                        for line in handle:
                            if line.startswith("#"):
                                continue
                            fields = line.strip().split(maxsplit=22)
                            print(fields)
                            this_hits.append(fields)

            orga_hits.update({organism: pd.DataFrame(this_hits, columns=attribs)})

            # break
# orga_hits.to_pickle("hmm_dom_orga_hits.pkl")
with open("hmm_dom_orga_hits.pkl", "wb") as handle:
    pickle.dump(orga_hits, handle)
# test_df = orga_hits["Drosophila_melanogaster.protein"]
