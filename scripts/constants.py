#!/usr/bin/env python3
"""
@author: Jannis de Riz
constants.py
"""
import os

CWD = os.getcwd()
# read Domain Hits tables and count
TAXA = [
    f

    for f in os.listdir("proteome")

    if os.path.isdir(os.path.join(CWD + "/proteome", f))
]
DOMAINS = [domain for domain in os.listdir("Domains") if domain.endswith(".hmm")]

CBP = ["Bromodomain", "zf-TAZ", "ZZ", "KIX", "Acetyltransf_10", "PAS_11", "HAT_KAT11"]
P300 = ["Bromodomain", "zf-TAZ", "ZZ", "KIX", "HAT_KAT11", "DUF902", "Creb_binding"]
KAT2A = ["Bromodomain", "Acetyltransf_1"]
SETD1B = [
    "N-SET",
    "SET",
    "RRM_1",
]  # TODO: find missing
SUV39H1 = ["Pre-SET", "SET", "Chromo"]
EHMT1 = ["SET", "Pre-SET", "Ank"]
EHMT2 = ["SET", "Pre-SET", "Ank"]
PROTEIN_COMPOSITION = {
    "CBP": CBP,
    "P300": P300,
    "KAT2A": KAT2A,
    "SETD1B": SETD1B,
    "SUV39H1": SUV39H1,
    "EHMT1": EHMT1,
    "EHMT2": EHMT2,
}
