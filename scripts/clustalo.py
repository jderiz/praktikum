"""
@author: Jannis de Riz
"""
import os

for msa in os.listdir(os.getcwd()):

    print(msa)

    if msa.endswith(".fasta"):
        command = "clustalo -i " + msa + " -t Protein -o msa/" + msa
        print(command)
        os.popen(command)
