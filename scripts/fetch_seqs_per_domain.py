"""
@author: Jannis de Riz
"""
import os
import pickle
import re
import subprocess
import sys

import pandas as pd

import constants as CONST

# get domain profiles from all organisms.
with open("orga_dom_by_target.pkl", "rb") as handle:
    orga_domain_profiles = pickle.load(handle)

print(CONST.PROTEIN_COMPOSITION)
skip_build = False
# ITERATE THROUGH PROTEIN_COMPOSITIONS

for protein_name, composition in CONST.PROTEIN_COMPOSITION.items():

    taxa_appearance = set()
    domain_buffers = {}

    with open("hmm_protein_seqs/" + protein_name + ".fasta", "w") as handle:
        print("fetch sequence for hmm Hits")

        if not skip_build:
            for organism, profile in orga_domain_profiles.items():  # ALLE ORGAS
                orga_domains = set(profile.index.to_list())

                if set(composition).issubset(orga_domains):
                    # all domains found
                    orga_fasta_path = (  # find the proteom sequence file for the organism
                        subprocess.check_output(
                            "find . -iname '{}*.faa'".format(organism), shell=True
                        )
                        .splitlines()[0]
                        .decode("utf-8")
                    )
                    # get Taxa for organism
                    Taxa = orga_fasta_path.split("/")[2]
                    taxa_appearance.add(Taxa)

                    # get all domains contained in protein
                    protein_profile = profile.loc[composition]

                    for domain, domain_hit in protein_profile.iterrows():
                        command = (
                            "esl-sfetch " + orga_fasta_path + " " + domain_hit.target
                        )
                        p = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
                        preprocessed, _ = p.communicate()
                        renamed = preprocessed.decode("utf-8")
                        # use organism as identifier
                        # try to make orga identifier unique.
                        orga_name = organism.split(".")

                        if len(orga_name) > 2:
                            orga_name = organism
                        else:
                            orga_name = orga_name[0]
                        renamed = re.sub(
                            r"^>.*", ">" + Taxa + "__" + orga_name, renamed
                        )

                        if "*" in renamed:
                            # get rid of old fasta style * for end of sequence
                            renamed = re.sub("[*]", "", renamed)
                        processed = renamed.encode("utf-8")

                        if domain in domain_buffers.keys():
                            print(processed.splitlines()[0])
                            domain_buffers[domain] += processed
                        else:
                            domain_buffers.update({domain: processed})
            # domain buffer holds domains of protein
            # for each organism that has this specific protein
            # baue aus buffern ein "protein alignment"
            domains_aligned = {}

            print("Align domains with clustalo")

            for dom, buffer in domain_buffers.items():
                # align with clustalo
                command = "clustalo -i - -t Protein --wrap=100000"  # input über stdin
                p = subprocess.Popen(
                    command.split(), stdin=subprocess.PIPE, stdout=subprocess.PIPE
                )
                res, _ = p.communicate(input=buffer)
                domains_aligned.update({dom: res})

            with open("hmm_protein_seqs/" + protein_name + "_domains.pkl", "wb") as pkl:
                pickle.dump(domains_aligned, pkl)

        print("build protein alignment from domains")

        # if skip build get aligned domains for protein from pickled file

        if skip_build:
            with open("hmm_protein_seqs/" + protein_name + "_domains.pkl", "rb") as pkl:
                domains_aligned = pickle.load(pkl)

        protein_alignments = {}

        max_ident_count = len(domains_aligned)

        for dom, dom_alignment in domains_aligned.items():
            ident_count = {}

            for line in dom_alignment.splitlines():
                line = line.decode("utf-8")

                if line.startswith(">"):
                    identifier = line

                    # add new keys

                    if line not in protein_alignments.keys():
                        protein_alignments.update({line: ""})

                    if line not in ident_count.keys():
                        ident_count.update({line: 1})
                else:  # APPEND ALIGNMENT TO ORGANISM IDENTIFIER
                    if ident_count[identifier] > 3:
                        print("caught")

                        continue
                    print(identifier, "LENGTH: ", len(protein_alignments[identifier]))

                    ident_count[identifier] += 1
                    protein_alignments[identifier] += line

        # schreibe im wechsel identifier und alginment in zeile der Protein Datei

        last_align = None

        for identifier, alignment in protein_alignments.items():
            # assure equal length per alignment

            if last_align is not None and len(last_align) != len(alignment):
                print("jetzt wirds richtig wild")
                print(identifier, len(alignment), len(last_align))
            handle.write(identifier)
            handle.write("\n")
            print(len(alignment), "  FOR:  ", identifier)
            handle.write(alignment)
            handle.write("\n")
            last_align = alignment

    # say in which Taxa some protein was found concerning hmmsearch
    print("\n \n \n", protein_name, "Found in ", taxa_appearance, "\n \n \n")
