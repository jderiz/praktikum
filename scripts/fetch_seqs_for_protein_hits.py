"""
@author: Jannis de Riz
"""
import os
import pickle
import re
import subprocess
import sys

import pandas as pd

import constants as CONST

# get domain profiles from all organisms.
with open("orga_dom_by_target.pkl", "rb") as handle:
    orga_dom_by_target = pickle.load(handle)

print(CONST.PROTEIN_COMPOSITION)
skip_build = False
# ITERATE THROUGH PROTEIN_COMPOSITIONS

for protein_name, composition in CONST.PROTEIN_COMPOSITION.items():

    taxa_appearance = set()
    protein_buffer = bytes()

    with open("hmm_protein_hits/" + protein_name + ".fasta", "wb") as handle:
        print("fetch sequence for hmm Hits")

        if not skip_build:
            for organism, targets in orga_dom_by_target.items():  # ALLE ORGAS
                hit_targets = {}

                best_eval = 100000
                best_target = None

                for target_ident, group in targets:
                    if set(composition).issubset(
                        set(group.domain)
                    ):  # found all domains
                        print(
                            target_ident,
                            " in " + organism + " contains all domains for ",
                            protein_name,
                        )

                        # chose "best" hit if multiple

                        if group.dom_i_eval.sum() < best_eval:
                            print(group.dom_i_eval.sum(), " better then ", best_eval)
                            best_eval = group.dom_i_eval.sum()
                            best_target = target_ident

                if best_target:  # if target found append to protein buffer
                    print(best_target)

                    orga_fasta_path = (  # find the proteom sequence file for the organism
                        subprocess.check_output(
                            "find . -iname '{}*.faa'".format(organism), shell=True
                        )
                        .splitlines()[0]
                        .decode("utf-8")
                    )
                    Taxa = orga_fasta_path.split("/")[2]
                    taxa_appearance.add(Taxa)
                    command = "esl-sfetch " + orga_fasta_path + " " + best_target
                    p = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
                    preprocessed, _ = p.communicate()
                    renamed = preprocessed.decode("utf-8")
                    orga_name = organism.split(".")

                    if len(orga_name) > 2:
                        orga_name = organism
                    else:
                        orga_name = orga_name[0]
                    renamed = re.sub(r"^>.*", ">" + Taxa + "__" + orga_name, renamed)

                    if "*" in renamed:
                        # get rid of old fasta style * for end of sequence
                        renamed = re.sub("[*]", "", renamed)
                    processed = renamed.encode("utf-8")
                    # print(processed)
                    protein_buffer += processed

            print(len(protein_buffer))

            if not protein_buffer == b"":  # check buffer empty
                print("Align Targets for protein {} with clustalo".format(protein_name))
                command = " clustalo -i - -t Protein --wrap=100000"
                p = subprocess.Popen(
                    command.split(), stdin=subprocess.PIPE, stdout=subprocess.PIPE
                )
                alignment, _ = p.communicate(input=protein_buffer)
                handle.write(alignment)
                print(
                    "\n \n \n", protein_name, "Found in ", taxa_appearance, "\n \n \n"
                )
