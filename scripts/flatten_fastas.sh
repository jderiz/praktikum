for fasta in */*.faa
do
	echo ${fasta}	
	# awk '/^>/ {printf("\n%s\n",$0);next; } { printf("%s",$0);}  END {printf("\n");}' < ${fasta} | tail -n +2 > ../proteome/${fasta};
	perl -pe '$. > 1 and /^>/ ? print "\n" : chomp' ${fasta} > ../proteome/${fasta}
done
