# for domain in CONST.DOMAINS:
#     print(domain)
#     domain = domain[:-4]
#     domain_fasta = domain + ".fasta"
#     with open("domainAlignments/" + domain_fasta, "w") as out_handle:

#         for organism, profile in orga_domain_profiles.items():  # the domains per organism
#             # print(profile)

#             if domain in profile.index:
#                 target = profile.loc[domain][
#                     "target"
#                 ]  # the identifier where the domain hit was made
#                 from_to = (
#                     profile.loc[domain][
#                         "alifrom"
#                     ]  # the range in the identifier sequence
#                     + ".."
#                     + profile.loc[domain]["alito"]
#                 )
#                 orga_fasta_path = (  # find the proteom sequence file for the organism
#                     subprocess.check_output(
#                         "find . -iname '{}*.faa'".format(organism), shell=True
#                     )
#                     .splitlines()[0]
#                     .decode("utf-8")
#                 )
#                 command = (  # fetch sequence of the envelope where the hit has been made from the organism proteom
#                     "esl-sfetch "
#                     # + from_to
#                     # + " "
#                     + orga_fasta_path
#                     + " "
#                     + target
#                 )
#                 print(command)
#                 p = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
#                 preprocessed, _ = p.communicate()
#                 # print(preprocessed.decode("utf-8"))
#                 # out_handle.write(preprocessed.decode("utf-8"))

#     # break  # Only One domain to Test
