#aus proteome heraus starten...
#ordner(){
#find . -type d | grep ./
#}
i=0 
for D in */*.faa
do
i=$(( $i + 1 ))
mkdir ../blastdb
makeblastdb -in ./${D} -out ../blastdb/${i} -dbtype prot -parse_seqids
g=0
for protein in ../7protein_refseqs/*.faa
do
g=$(( $g + 1 ))
blastall -p blastp -d ../blastdb/${i} < ${protein} > ../blastOUTPUT/${i}-${g}.out
done
rm -r ../blastdb/
done
