#! /bin/env/python
"""
@author: Jannis de Riz
"""

import os

import constants as CONST

"""
For all Domain HMM search against every Organism DB
"""
CWD = os.getcwd()
DOMAIN_HMMS = CONST.DOMAINS
TAXA = CONST.TAXA
print(TAXA)
print(DOMAIN_HMMS)

for hmm in DOMAIN_HMMS:  # For all Domains
    if not hmm.endswith("DS_Store"):
        for taxon in TAXA:  # Iteratre through taxa
            for organism in os.listdir(
                "proteome/" + taxon
            ):  # Iterate through organisms in TAXA

                if not organism.lower().endswith(
                    tuple(["mis", "missing", ".ssi"])
                ) and not organism.startswith("missing"):
                    org_fasta = "/" + organism
                    org_dir = (
                        org_fasta[:-4] + "/" + hmm[:-4]
                    )  # no postfix for directories
                    os.makedirs(
                        "hmmsearchOUT/" + taxon + org_dir, exist_ok=True
                    )  # assure directories
                    folder = "hmmsearchOUT/" + taxon + org_dir
                    res = os.popen(
                        "hmmsearch --notextw -A "
                        + folder
                        + "/hitsAlign.sto"
                        + " --tblout "
                        + folder
                        + "/seqHits.tbl"  # per sequence Hits
                        + " --domtblout "
                        + folder
                        + "/domHits.dtbl"  # per Domain Hits
                        + " Domains/"
                        + hmm
                        + " proteome/"
                        + taxon
                        + org_fasta
                        + " >"
                        + folder
                        + "/hmm.out"  # general output
                    )
