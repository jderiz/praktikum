\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {section}{\numberline {2}Methods}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Blast}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}HMMER}{3}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Splitstree}{3}{subsection.2.3}
\contentsline {section}{\numberline {3}Results}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Comparison HMMER and BLAST Hits}{4}{subsection.3.1}
\contentsline {paragraph}{EHMT1/2}{4}{section*.3}
\contentsline {paragraph}{KAT2A}{4}{section*.4}
\contentsline {paragraph}{SETD1B}{5}{section*.5}
\contentsline {paragraph}{SUV39H1}{5}{section*.6}
\contentsline {paragraph}{P300}{5}{section*.7}
\contentsline {subsection}{\numberline {3.2}Comparison of Taxa and Protein evolution}{5}{subsection.3.2}
\contentsline {paragraph}{EHMT1/2}{5}{section*.8}
\contentsline {paragraph}{SETD1B}{5}{section*.9}
\contentsline {paragraph}{KAT2A}{5}{section*.10}
\contentsline {paragraph}{CBP}{6}{section*.11}
\contentsline {paragraph}{P300}{6}{section*.12}
\contentsline {section}{\numberline {4}Discussion and Outlook}{6}{section.4}
\contentsline {section}{\numberline {A}Figures}{8}{appendix.A}
