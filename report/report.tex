\documentclass{SeminarV2}
\usepackage[]{graphicx}
\usepackage[utf8]{inputenc}
\usepackage{amssymb,amsmath,array}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{appendix}
\usepackage{pdflscape}
\usepackage{subcaption}
\graphicspath{{../Trees/}}
%***********************************************************************
% !!!! IMPORTANT NOTICE ON TEXT MARGINS !!!!!
%***********************************************************************
%
% Please avoid using DVI2PDF or PS2PDF converters: some undesired
% shifting/scaling may occur when using these programs
% It is strongly recommended to use the DVIPS converters.
%
% Check that you have set the paper size to A4 (and NOT to letter) in your
% dvi2ps converter, in Adobe Acrobat if you use it, and in any printer driver
% that you could use.  You also have to disable the 'scale to fit paper' option
% of your printer driver.
%
% In any case, please check carefully that the final size of the top and
% bottom margins is 5.2 cm and of the left and right margins is 4.4 cm.
% It is your responsibility to verify this important requirement.  If these margin requirements and not fulfilled at the end of your file generation process, please use the following commands to correct them.  Otherwise, please do not modify these commands.
%
\voffset 0 cm \hoffset 0 cm \addtolength{\textwidth}{2cm}
\addtolength{\textheight}{2cm}\addtolength{\leftmargin}{0cm}

%***********************************************************************
% !!!! USE OF THE SeminarV2 LaTeX STYLE FILE !!!!!
%***********************************************************************
%
% Some commands are inserted in the following .tex example file.  Therefore to
% set up your Seminar submission, please use this file and modify it to insert
% your text, rather than staring from a blank .tex file.  In this way, you will
% have the commands inserted in the right place.

% Edited by Martin Bogdan.

\begin{document}
%style file for Seminar manuscripts
\title{Report for Group 8 considering Proteins\newline [EHMT1/2, CBP, P300, KAT2A, SETD1B, SUV39H1]}

%***********************************************************************
% AUTHORS INFORMATION AREA
%***********************************************************************
\author{Rebeca Guerra, 
Alexander Holzenleiter, 
 Jannis de Riz}
%***********************************************************************
% END OF AUTHORS INFORMATION AREA
%***********************************************************************

\maketitle

\tableofcontents
\listoffigures
\newpage
% \begin{abstract}
% Type your 100 words abstract here. Please do not modify the style
% of the paper. In particular, keep the text offsets to zero when
% possible (see above in this `SeminarV2.tex' sample file). You may
% \emph{slightly} modify it when necessary, but strictly respecting
% the margin requirements is mandatory (see the instructions to
% authors for more details).
% \end{abstract}

% ****************************************************************************
% MAIN
% ****************************************************************************
\section{Introduction}%
\label{sec:Introduction}

The combination of various approaches of genomic scale data with phylogenetic methods is known as phylogenomics. The development of phylogenomics has contributed to profound changes in the Eukaryote Tree of life (eToL), which represents the phylogeny of all eukaryotic lineages as well as the framework on which the origins and history of eukaryote biology and the evolutionary processes (cell biology, genome organization, sex, and multicellularity) underpinning it are understood \cite{burki2020new}. Epigenetic regulation of gene expression is one of the key mechanisms regulating cell-fate choices and cell identity during development \cite{schuettengruber2017genome}, such as posttranslational modifications of histones tails (hPTMs), which are important components of the regulatory genomic landscape in eukaryotes. Sebe-Pedros and others \cite{sebe2016dynamic} showed that histone posttranslational modifications, particularly those in H3 and H4, are highly conserved between \textit{Capsaspora owczarzaki}, a close extant unicellular relative of Metazoa, and animals and in other eukaryotes. Two of the diverse histone modifications in eukaryotic cells are histone acetylation and histone lysine methylation. The former is linked to active genes, whereas the latter is involved in both transcriptional activation and silencing \cite{cui2008histone}.

Histone acetyltransferases (HATs) are the key enzymes responsible for the acetylation of the histone tails and play important roles in the regulation of chromatin assembly, RNA transcription, DNA repair, and other DNA-templated reactions. HATs fall into at least four different families based on sequence conservation within the HAT domain, including Gcn5/PCAF, CBP/p300, Rtt109, and MYST families. KAT2A (GCN5) is a component of the ADA2A-containing complex (ATAC), a complex with histone acetyltransferase activity on histones H3 and H4. This protein is one of the most-studied HATs, conserved from Saccharomyces cerevisiae to humans, and carries two conserved domains: an acetyltransferase domain required for its catalytic activity and a bromodomain that binds acetylated lysine residues \cite{guelman2009double}; \cite{mitchell2019interpro}. CREB-binding protein (CBP) and p300 are highly conserved and functionally related transcription coactivators and histone/protein acetytransferases \cite{yuan2002acetyltransferase}. Acetylation of core histones gives an epigenetic tag for transcriptional activation. In addition to the HAT and bromo domains, both CBP and p300 mediate cAMP-gene regulation by binding specifically to phosphorylated CREB protein via the KIX domain \cite{uniprot2019uniprot}.

Dynamic lysine methylation plays fundamental roles in chromatin structure and gene expression in a wide range of eukaryotic organisms. Histone lysine modification is catalyzed by histone lysine methyltransferases (HKMTs)/SET Domain Group (SDG) proteins which commonly possess an evolutionarily conserved SET domain as a catalytic module, except for the non-SET-domain protein DOT1 \cite{cui2008histone}. The euchromatin histone methyltransferases (EHMTs) are an evolutionarily conserved protein family that are known for their ability to dimethylate histone 3 at lysine 9 in euchromatic regions of the genome, and are characterized by the presence of an N-terminal SET (interacts with WIZ) and Pre-SET (binds three zinc ions ) domain, preceded by a series of ankyrin (ANK) repeats which bind the monomethylated RELA subunit of NF-kappa-B \cite{kramer2016regulation}; \cite{mitchell2019interpro}. Trimethylation of histone 3 lysine 9 (H3K9me3), a repressed transcription mark, is mainly regulated by the histone lysine N-methyltransferases (HKMTs), SUV39H1 and SETDB1 \cite{spyropoulou2014role}. Along with SET and Post-SET (forms a zinc-binding site when coupled to a fourth conserved cysteine) domain, SUV39H1 and SETDB1 contain a pre-SET and chromatin organization modifier (chromo) domain, and a RNA recognition motif (RRM), respectively. The chromodomain of SUV39H1 is commonly found in proteins associated with remodeling and manipulation of chromatin. The RRM motif is probably diagnostic of an RNA binding protein. RRMs are found in a variety of RNA binding proteins, including various hnRNP proteins, proteins implicated in regulation of alternative splicing, and protein components of snRNPs \cite{mitchell2019interpro}.

\section{Methods}%
\label{sec:Methods}

\subsection{Blast}
\label{subsec:blast_methods}
We identified homologous candidates in the given 49 proteomes to investigate the phylogeny of our seven proteins using two approaches. The first approach was based on a search with Blast \cite{ altschul1990basic}.
For the implementation of Blast as a query we considered isoforms from the refseq database for \textit{Homo sapiens} with the lowest numbering, and simultaneously the shortest primary sequence relative to the other isoforms. Thus, making it easier to avoid possible optional domains. Only the hits obtained from the blast-search with an E-value less than $10^{-5} $were taken into consideration for further analysis. Furthermore, we set individual cutoffs for coverage for the query proteins, EHMT1 50\%, EHMT2 25\%, KAT2A 50\%, p300 20\%, SETD1B 45\% and SUV39H1 45\%. The established the value for p300 at only 20\%, because the primary sequence of our corresponding query was relatively long (2388 monomers) whereas the length of the other isoforms, except for CBP, was less than 500 aa. No significant hits were found for CBP with sufficient coverage in any of the proteomes (we rated hits with less than 10\% coverage as not useful for our implementation). After filtering the hits that met the aforementioned criteria, we selected the ones with the highest score per proteome and query. We subjected the resulting multi-fasta files to a manual check for meaningfulness. We double-checked our general procedure by performing a separate blast with the blast online application of the NCBI \href{https://blast.ncbi.nlm.nih.gov/Blast.cgi? PROGRAM=blastp&PAGE_TYPE=BlastSearch&LINK_LOC=blasthome}{Blast online}, as we were not expecting the absence of homologs in the proteomes for CBP (see the explanation in the discussion). The results obtained from the online application confirmed our findings.

\subsection{HMMER}
\label{subsec:hmmsearch}
Here, we explain the pipeline construction for finding our proteins by searching for their mandatory domains within the supplied proteoms using the hmmsearch commandline tool provided by \href{http://www.hmmer.org}{HMMER 3.3}.
We started by taking our previuos research results from \ref{sec:Introduction} and used them to create dictionairies containing the minimum domain composition for each given protein. For each domain a hmm profile was aquired by searching the \href{https://pfam.xfam.org/}{PFAM DAtabase} .
Then, a hmmsearch was run per domain against all Proteoms. This was done using the default hmmsearch cutoffs and settings.
The results where then reformatted into a pandas DataFrame for further use.
Significant Hits where fitlered in reference to \href{http://eddylab.org/software/hmmer/Userguide.pdf}{http://eddylab.org}. Meaning, a full sequence e-value of less then $10^{-3}  $had to be statisfied, as well as both domain e-values had to be less then 1. Also, the domainis bit score had to be higher than the domains bias. The resulting hits where grouped by their target sequences. Then the best target sequence in a organism, containing all mandatory domains of a protein, was fetched using esl-sfetch from \href{https://github.com/EddyRivasLab/easel}{easel Tools} and written to the protein fasta file. The quality of a target hit was determined by the sum of its domains independent e-values.

The code responsible for the hmmsearch and further analysis of the results can be inspected in the repository in: \href{https://gitlab.com/jderiz/praktikum/scripts}{/scripts} in hmmsearch.py reformat-hmmsearch.py make-profile-for-orga.py fetch-seqs-for-protein-hits.py.

\subsection{Splitstree}
\label{subsec:Estimating phylogenetic trees and networks using SplitsTree 4.16.1}

SplitsTree4 was inspired by Splitstree3, which was primarily an implementation of the split decomposition method. However, SplitsTree4 is a completely new program that integrates a wide range of phylogenetic network and phylogenetic tree methods, inference tools, data management utilities, and validation methods. Split decomposition can enhance the phylogenetic analysis of distance data by detecting opposite groupings ("splits") of organisms that are defined by distinctive distance features, caused by common ancestry, convergence, or systematic or random errors. Split networks are representations of splits, and using more splits permits a more accurate representation of the data[\cite{huson2006application}; \cite{bandelt1992split}.
The multi-sequence alignments (MSA) resulting from Blast and Hmmer hits were the initial input to the program, which arranges its data in a list of different “blocks” arranged in a specific order, namely the “processing pipeline”. Any phylogenetic method is viewed as a “transformation” of one type of data block to another. The “source” block was a “characters” block containing the Blast and Hmmer multi-sequence alignments for each of the seven proteins (14 MSA in total, 7 Blast, 7 Hmmer). Considering the available and applicable methods that would run, the 14 MSA were analyzed separately applying the following pipeline: the data in the characters block was transformed into a distances block using Hamming as distance calculation method. Next, the distances block was converted into a Tree using BioNJ, an improved version of the neighbor-joining (NJ) algorithm. Subsequently, the Trees block transforms the trees output from the distances block into splits. Finally, using the phylogram algorithm to compute a traditional phylogenetic tree the data in the splits block was visualized as a cladogram. 
It is important to note, that version 4.16 has problems with input in multi-fasta format and/or with the reconstruction of phylogenetic networks \href{https://software-ab.informatik.uni-tuebingen.de/download/splitstree4/release_notes.txt}{Splitstree4 Release notes}.

\section{Results}%
\label{sec:Results}
As already state in Section~\ref{sec:Methods}, there was no meaningfull alignment for the CBP Blast search possibel due to insufficient hit quality. Therefore, only six Protein trees where generated using the Blast method. In general the Bootstrap support for the trees generated with Blast is very weak. Therefore they where not consulted in our final analysis. The Bootstrap support for the Hmmer trees varies. Inner nodes showed good to optimal support but outer nodes support varied from as low as 18 to up to 100.
\subsection{Comparison HMMER and BLAST Hits}%
\label{sub:comp_hmm_blast}
Beforehand, there is to say that HMMER, given our settings and filterings as described in subsection~\ref{subsec:hmmsearch}, has found significant hits for CBP in Contrast to BLAST. When running protein composition hmmsearch against the entire proteom of a species rather then again each proteom sequence of it, significantly more hits where found. Stating, that generally all the mandatory domains are well present in the proteom but are not expressed in a single sequence.

\paragraph{EHMT1/2}%
\label{par:EHMT}
Compared to the BLAST results hmmsearch yielded less hits resulting in a smaller tree. The HMMER trees of EHMT1/2 are homologue in their strucure with moderate bootstrapping values in early and strong values in later knots. The BLAST trees diverge in several places. The low Bootstrap values of the BLAST cladograms in Figure~\ref{fig:EHMT1_cladogramBlast} and \ref{fig:EHMT2_cladogramBlast} indicate conflicting or little signal in the alignment. Interestingly the \textit{Drosophila melanogaster} Branch has very strong signal and is seperated from the other Bilateria organism in BLAST but does appear in the hmmsearch as neighbours within its taxon group in a different place, See Figures~\ref{fig:EHMT1_cladogramHmmer} and \ref{fig:EHMT2_cladogramHmmer}.

\paragraph{KAT2A}%
\label{par:KAT2A}
One of the most prominent differences in the HMMER and BLAST trees for KAT2A is that BLAST did not find any hits for Fungi or Teretosporea but hmmsearch did. Again, following paragraph \ref{par:EHMT} the Taxa phyleticity differs. In contrast to the hmmersearch results the BLAST Alignment shows several different taxa within its neighbouring leaves making those clades for this specific tree polyphyletic. See Figures~\ref{fig:KAT2A_cladogramBlast} and \ref{fig:KAT2A_cladogramHmmer} for reference.

\paragraph{SETD1B}%
\label{par:SETD1B}
The SETD1B trees have several differences in clade composition. Most obviously is the single leave clade at the very top which is Bilateria in BLAST and Porifera in HMMER. It appears that Prorifera and Bilateria have somewhat swapped positions. Placing Porifera near Placoazoa in BLAST and Bilateria - to some extend - near Placoazoa in HMMER respectively. It is to mention that the BLAST search did not yield any hits for Choanoflagellata in contrast to HMMER. The Cnidaria calade is similarly represented in both results. Also, there is the exact Ctenophora hit found in HMMER and BLAST with \textit{Mnemiopsis leidyi}. The two trees are displayed in Figure~\ref{fig:SETD1B}

\paragraph{SUV39H1}%
\label{par:SjUV39H1}
Although HMMER only hit 4 targets concerning the SUV39H1 Protein it should still be mentioned for the sake of completeness.
BLAST search resultet in plenty hits for this Protein which could give reason for further analysis on the matter. Also to mention, HMMER places Porifera Sycon ciliatum again on the outgroup in contrast to BLAST which has Bilateria Drosophila melanogaster on that position. See Figure~\ref{fig:SUV39H1} for reference.

\paragraph{P300}%
\label{par:P300}
As for the P300 Protein BLAST gets generally more hits which might be due to its cutoff settings described in \ref{subsec:blast_methods}. Most prominent again is the mostly monophyletic clade structure for the Hmmer results in contrast to the more polyphyletic one in BLAST. Another recurrent difference is the outgroup. BLAST results put there again \textit{Drosophila melanogaster}, separating the Bilateria taxa. Hmmer results show \textit{Ephydatia muelleri} of the Porifera taxa as the outgroup. The P300 trees can be seen in Figures~\ref{fig:p300_cladogramBlast} and \ref{fig:P300_cladogramHmmer}.

\subsection{Comparison of Taxa and Protein evolution}
For the comparison of taxa and protein evolution we will soley consider the trees generated by Hmmer. This is due to their superior fittness indicated by the Bootstrap results.
\paragraph{EHMT1/2}
As for EHMT1 \ref{fig:EHMT1_cladogramHmmer} and EHMT2 \ref{fig:EHMT2_cladogramHmmer}, they show similar results as their mandatory protein composition is the same. They contain all 7 Cnidaria from the proteom set. Further, the two closely related species regarding the Phylogenetic taxa tree from the Bilateria clade are present. It strikes the eye that only two of six species from Porifera are represented in the trees. Namely, \textit{Sycon ciliatum} as well as \textit{Oscarella carmella}. Also \textit{Placozoa Trichoplax sphH2} is positioned within a clade with the two Bilateria although they are placed far apart in the phylogeny concerning the species. This is espacially true in relation to the other species in the EHMT1 and EHMT2 trees.
\paragraph{SETD1B}
Looking at SETD1B \ref{fig:SETD1B_cladogramHmmer} we see the clades of Cnidaria and Choanoflagellata cleary portrait. Nevertheless, \textit{Filasterea Capsaspora owczarzaki} was placed within the Choanoflagellata and \textit{Cnidaria Hydra vulgaris} far of Choanoflagellata. Both examined Bilateria are placed far apart from each other.
\paragraph{KAT2A}
The KAT2A \ref{fig:KAT2A_cladogramHmmer} tree contains 42 of the 49 proteoms at hand. It thereby is the tree with the distinguishly most hits from the Hmmer runs. The tree clearly displays the clades of Choanoflagellata and Cnidaria. Yet, \textit{Choanoflagellata Salpingoeca kvevrii} is positioned in a clade with two Fungi. \textit{Porifera Sycon ciliatum} and \textit{Oscarella carmella} branch of at the most inner knode and lie well spearated from the other Porifera.

\paragraph{CBP}
When looking at the CPB tree \ref{fig:CBP_cladogramHmmer} only three out of the six Porifera can be seen. The ones present are not in a clade together. Rather, \textit{Oscarella carmella} can be found in the Cnidaria clade. As for \textit{Amphimedon queenslandica} as well as\textit{Ephydatia muelleri} they are both appearing as an outgroup to the rest of the tree. The two Placozoa are located within a clade paired with  \textit{Cnidaria Hydra vulgaris} and close to the rest of Cnidaria. The two Bilateria are seperated and \textit{Drosophila melanogaster} contradicts its phylogenetic position. The examined species of Filasterea is located within the Choanoflagellata clade which contradicts its phylogenetic position as well.
\paragraph{P300}
The p300 \ref{fig:P300_cladogramHmmer} tree is identical to the one for CBP.
\section{Discussion and Outlook}%
\label{sec:Discussion and Outlook}


Based on the very poor bootstrap support for the trees based on the Blast data set, we can only consider the results of the Hmmer implementation. Moreover, the Blast and Hmmer results differ significantly to some extent. The poor bootstrap support could be a strong indication of erroneous sequences in the data sets. Unsuitable candidates, which probably only show sequence similarity in subsections or individual domains, but instead contain other domains could lead to the fatal bootstrap results. Another explanation might be a strong difference in the primary sequence between the query isoforms and the actual homologs. For example, due to diverse splicing, where exons could be exchanged or otherwise differently allocated in the primary sequence. In contrast, for the Hmmer implementation single domains, were searched and aligned in the respective order, which might explain the much better bootstrap support (also, Hmmer search is generally more reliable than a Blast search).

As p300 and CBP are closely realted, their Hmmer trees share the same composition. The clades concerning the species phylogeny are well presented. The representations for Ctenophora and Filasterea are present. Yet, \textit{Filasterea Capsaspora owczarzaki} is not correctly positioned. This leads to the presumtion that, due to weak Bootstrapping for the branch, there are erroneous results here. If there was more time, we would have included several more species of Ctenophora, Filasterea and Ichthyosporea in order to get more robust results. Nevertheless \cite{schuettengruber2017genome} does confirm our findings to some extend as in that we found CBP homologues in all our proteoms except Fungi and Plantae.


The trees for EHMT1 and EHMT2 show similar structure and composition, which can be explained due to the close relationship of both proteins. Both proteins were found in Cnidaria and Bilateria. As for the other clades, the proteins were found in only one of the two examined Placozoa, in two of the six Porifera species, and one Ctenophora, but this species was unexpectedly classified in the Cnidaria clade. These three species groups (Placozoa, Porifera, and Ctenophora) are the closest to the Cnidaria and Bilateria concerning the other inspected groups. Together these five groups form the Metazoa clade. Therefore, it could be assumed that EHMT1 and EHMT2 represent a phylogenetic innovation regarding the formation or early evolutionary history of the Metazoa. However, this assumption is not sufficiently supported by our results. As mentioned before, EHMT1/2 was not found in all investigated species of Placozoa and Porifera, besides the bootstrap support for the trees is insufficient and lastly, neighbor-joining was used as a reconstruction method instead of more reliable methods like Maximum Likelihood or Bayesian Inference methods. A more thorough investigation must be conducted to clarify this assumption. To further the investigation, we could carry out a more thorough selection (including a manual selection) of candidate proteomes, including significantly more suitable proteomes. At this point, it may be appropriate to investigate proteins that are functionally closely related to the action of EHMT1/2. Furthermore, we could generate the trees by Maximum Likelihood, conduct a more intensive literature search, and possibly get in contact with expert scientists on these topics.

Similarly, the results for SETD1B do not show a clear picture. The assumption that the protein is a phylogenetic novelty in the formation or early evolutionary history of the Metazoa is contradicted by the four Choanoflagellata found in the Hmmer tree. Given that no Choanoflagellata were found with the blast search, it could be that these are erroneous hits. Nevertheless, maybe more species from this family tree should be taken into consideration for the analysis to make reliable claims about the phylogeny of SETD1B. However, our investigation suggests that the protein first appeared in the direct unicellular ancestors of Metazoa or the last common Metazoa ancestor.

The KAT2A Hmmer tree presented a reasonable order according to the phylogeny of the species, except for the classification of \textit{Choanoflagellata Salpingoeca kvevrii}, \textit{Porifera Sycon ciliatum} and \textit{Oscarella carmella}. These three sequences could represent erroneous hits and have a moderate effect on the appearance of the tree. KAT2A was also found in Fungi, Viridiplantae, and Filasteria, indicating that KAT2A can also be found outside of the Metazoa and their close unicellular relatives. These findings would mean that it is not an evolutionary novelty of the Metazoa. However, only three species (two Fungi and one Viridiplantae) were selected as outgroup from the 49 proteomes. With more time, we could involve considerably more species in the analysis with the hopes of obtaining more reliable results.



\newpage
\appendix
	
\section{Figures}%
\label{sec:Figures}
See also \href{https://gitlab.com/jderiz/praktikum/-/tree/master/Trees}{Trees} for full size Pictures.
%%%%%clip=true trim={l b r t}
\begin{figure}[ht]
	\centering
	\includegraphics[scale=0.2, trim={40cm 0 25cm 0}]{CBP_cladogramHmmer.pdf}
	\caption{CBP HMMER}%
	\label{fig:CBP_cladogramHmmer}
\end{figure}

\addtolength{\textheight}{2cm}
\addtolength{\textwidth}{4cm}
\addtolength{\rightmargin}{4cm}
\begin{landscape}
\begin{figure}
	\vspace{-5cm}
	\begin{subfigure}[ht]{.5\linewidth}
	\centering
	\includegraphics[width=\linewidth, trim={35cm 0 25cm 0}]{EHMT1_cladogramBlast.pdf}
	\caption{EHMT1 BLAST}%
	\label{fig:EHMT1_cladogramBlast}
\end{subfigure}
~
\begin{subfigure}[ht]{.5\linewidth}
	\centering
	\includegraphics[width=\linewidth, trim={25cm 0 35cm 0}, clip=true]{EHMT1_cladogramHmmer.pdf}
	\caption{EHMT1 HMMER}%
	\label{fig:EHMT1_cladogramHmmer}
\end{subfigure}	
\caption{EHMT1}
\end{figure}

\begin{figure}
	\vspace{-5cm}
\begin{subfigure}[ht]{.5\linewidth}
	\centering
	\includegraphics[width=\linewidth, trim={35cm 0 25cm 0cm}]{EHMT2_cladogramBlast.pdf}
	\caption{EHMT2 BLAST}%
	\label{fig:EHMT2_cladogramBlast}
\end{subfigure}
~
\begin{subfigure}[ht]{.5\linewidth}
	\centering
	\includegraphics[width=\linewidth, trim={20cm 0 35cm 0}, clip=true]{EHMT2_cladogramHmmer.pdf}
	\hspace{-2cm}
	\caption{EHMT2 HMMER}%
	\label{fig:EHMT2_cladogramHmmer}
\end{subfigure}	
\caption{EHMT2}
\end{figure}

\begin{figure}
	\vspace{-5cm}
\begin{subfigure}[ht]{.5\linewidth}
	\centering
	\includegraphics[width=\linewidth, trim={25cm 0 25cm 0cm}, clip=true]{SETD1B_cladogramBlast.pdf}
	\caption{SETD1B BLAST}%
	\label{fig:SETD1B_cladogramBlast}
\end{subfigure}
~
\begin{subfigure}[ht]{.5\linewidth}
	\centering
	\includegraphics[width=\linewidth, trim={30cm 0 25cm 0}, clip=true]{SETD1B_cladogramHmmer.pdf}
	\caption{SETD1B HMMER}%
	\label{fig:SETD1B_cladogramHmmer}
	\hspace{-5cm}
\end{subfigure}
\caption{SETD1B}
\label{fig:SETD1B}
\end{figure}

\begin{figure}
	\vspace{-5cm}
\begin{subfigure}[ht]{.5\linewidth}
	\centering
	\includegraphics[width=\linewidth, trim={30cm 0 25cm 0}, clip=true]{KAT2A_cladogramBlast.pdf}
	\caption{KAT2A BLAST}%
	\label{fig:KAT2A_cladogramBlast}
\end{subfigure}
~
\begin{subfigure}[ht]{.5\linewidth}
	\centering
	\includegraphics[width=\linewidth, trim={25cm 0 25cm 0}, clip=true]{KAT2A_cladogramHmmer.pdf}
	\caption{KAT2A HMMER}%
	\label{fig:KAT2A_cladogramHmmer}
	\hspace{-5cm}
\end{subfigure}
\caption{KAT2A}
\label{fig:KAT2A}
\end{figure}

\begin{figure}
	\vspace{-5cm}
\begin{subfigure}[ht]{.5\linewidth}
	\centering
	\includegraphics[width=\linewidth, trim={34cm 0 25cm 0}, clip=true]{SUV39H1_cladogramBlast.pdf}
	\caption{SUV39H1 BLAST}%
	\label{fig:SUV39H1_cladogramBlast}
\end{subfigure}
~
\begin{subfigure}[ht]{.5\linewidth}
	\centering
	\includegraphics[width=\linewidth, trim={15cm 0 25cm 0}, clip=true]{SUV39H1_cladogramHmmer.pdf}
	\caption{SUV39H1 HMMER}%
\end{subfigure}
\caption{SUV39H1}
\label{fig:SUV39H1}
\end{figure}

\begin{figure}
	\vspace{-7cm}
\begin{subfigure}[ht]{.5\linewidth}
	\centering
	\includegraphics[width=\linewidth, trim={30cm 0 30cm 0}]{p300_cladogramBlast.pdf}
	\caption{P300 BLAST}%
	\label{fig:p300_cladogramBlast}
\end{subfigure}
~
\begin{subfigure}[ht]{.5\linewidth}
	\centering
	\includegraphics[width=\linewidth, trim={25cm 0 25cm 0}, clip=true]{P300_cladogrammHmmer.pdf}
	\caption{P300 Hmmer}
	\label{fig:P300_cladogramHmmer}
\end{subfigure}
\caption{P300}%
\end{figure}

\end{landscape}
% ****************************************************************************
% BIBLIOGRAPHY AREA
% ****************************************************************************

\begin{footnotesize}

% IF YOU USE BIBTEX,
% - DELETE THE TEXT BETWEEN THE TWO ABOVE DASHED LINES
% - UNCOMMENT THE NEXT TWO LINES AND REPLACE 'Name_Of_Your_BibFile'

\bibliographystyle{unsrt}
\bibliography{bib}

\end{footnotesize}

% ****************************************************************************
% END OF BIBLIOGRAPHY AREA
% ****************************************************************************

\end{document}
