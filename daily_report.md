Bitte schreibt in diese Datei am Ende des Tages was ihr alles gemacht habt, einerseits für euch, damit damit ihr ein überblich habt eas ihr alles geschafft habt, andererseits für die Tutoren, damit wir wissen wo ihr jeweils grade steht.

Diese Datei soll also am Ende jeden Tages committed und gepushed werden.

In dem report sollten stehen: 

* ein Datum
* Erkenntnisse und was ihr gelernt habt, insbesondere über eure Proteine.
* Verweise auf Graphiken und Bäume die ihr erstellt habt.
* Offene Probleme sowie  Aufgaben und Ideen wie ihr damit weitermachen werdet.
* aus dem protoll darf gerne hervorgehen wer an was gearbeitet hat, insbesondere dann, wenn ihr das arbeitsergebnis verschiedener Personen  über einen account committed. 

Montag 22/06

weil es heute vielleicht noch nicht so viel zu berichten gibt, haben wir euch ein paar fragen vorbereitet, die ihr beantworten sollt:

A) Welche Rolle spielen die ihnen zugewiesenen Proteine in der epigenetischen Regulation?
   Welchen Komplexen gehoeren Sie an? Welche funktion haben Sie?

B) Kennzeichnen Sie im Speziesbaum MetazoaOri_topo die folgenden Taxa:
Bilateria, Choanoflagellata, Cnidaria,  Corallochytrea, Filasterea, Fungi, Ichthyosporea, Placozoa, Plants, Porifera, Teretrosporea

C) Welche Auswirkung koennten die Unterschiede der Datenquellen auf das Ergebnis haben?
 
D) Welches der ihnen zugewiesenen Proteine hat die meisten, unterschiedlichen Domänen? 
   Zu welchen davon gibt es PFAM modelle?


# Tag 1 22.06


Wir machten uns mit Git, sowie intensiv mit den gegebenen Dateien und Taxa vertraut. 
Danach richteten wir unsere "Proteom-Bibliothek" ein. 

Bezüglich der Referenzsequenzen für unsere 7 Proteine haben wir uns für Sequenzen aus der RefSeq-DB zum Menschen entschieden. 
Wir berücksichtigten jeweils jene Isoform, die relativ groß bezüglich der anderen war und möglichst die erstnummerierte ist. 
Lediglich für WHMT1 fanden wir bisher keine Sequenzen oder weiteren Informationen. Wir suchten in Genecards, Interpro, NCBI, Kegg und String-DB, 
bis wir auf die Idee kamen, dass das W für Wildtyp stehen könnte, was unsere Lösung darstellte und in einer erfolgreichen Recherche zu HMT1 resultierte.  
 

RECHERCHE ZU UNSEREN PROTEINEN:

[CBP](proteins/CBP)
[EHMT2](proteins/EHMT2)
[EHMT1](proteins/EHMT1)
[p300](proteins/p300)
[KAT2A](proteins/KAT2A)
[SETD1B](proteins/SETD1B)
[SUV39H1](proteins/SUV39H1)


# Tag 2 23.06


Heute ging es darum die Alignmentsuche mit BLAST zu automatisieren. 
Wir haben bei der Automatisierung sowohl ein Pythonscript als auch ein Shellscript (parallel) probiert. Beide Skripte sind zu '99 %' gelungen, man müsste nur an einer technischen Kleinigkeit arbeiten.
Dementsprechend würden wir gerne morgen Vormittag eine der Lehrenden/Tutoren kontaktieren um die technische Kleinigkeit loszuwerden.


# Tag 3 24.06

Die Alignmentsuche mit BLAST wurde abgeschlossen. 
Die Hits werden momentan analysiert und die besten pro Protein werden in der
pipeline weiter mit clustalo alignt. Diese ergebnisse werden dann mit splitstree
untersucht.


# Tag 4 25.06

Die Hits der Aligments mit BLAST wurden weiter analysiert. Nach dem Gespräch mit den Tutoren/Lehrenden haben wir uns Gedanken gemacht wie wir am besten mit den Hits handeln (auswählen) für die weiteren Schritte.
Es wurde ein Script für die automatisierte Suche aller Domänen in allen Organismen mit hmmer.Ein Script zum parsen der Outputs von hmmer wurde auch angefangen.
Die HMM der Dömanen für die 7 Proteine wurden herunter geladen.


#Tag 5 26.06

Wir haben heute weiter die Blast-Ergebnisse analysiert.
Leider hatte sich dabei ein Skript-Fehler eingeschlichen, bei dem 
ein Zeilen-Shift eintrat und dadurch falsche Scores eingelesen 
wurden, die zu komischen Ergebnissen führten. Dies hatte uns viel Zeit gekostet.
Durch den wertvollen Tipp, das das Manual unter https://www.ncbi.nlm.nih.gov/Class/BLAST/blastallopts.txt 
unvollständig ist (es enthält nicht die Optionen für einen geeigneteren Output), 
konnten wir deutlich besser verarbeitbare Outputs generieren. 

Laut dem Paper von Schuettengruber müssten wir CBP in fast allen unserer 49 Proteome finden.
Allerdings ist dem wohl nicht so. Erst, wenn der Coverage-cutoff von 15% auf 10% heruntergesetzt wird (was sehr wenig ist, 
wie wir finden) erhöht sich die Zahl repräsentierter Taxa von 1 auf 46. Montag könnten wir untersuchen, ob es sich dabei um 
eine einzelne Domäne handelt und welche dies ist.
Generell stellten wir fest, dass wir den Coverage-Cutoff niedrig setzen mussten, um Treffer in den Genomen zu bekommen.
Hier dazu eine Tabelle:

Protein		Coverage-Cut-Off (%)	Anzahl Proteome mit erfolgloser Suche
CBP			/  (10)					48 (3)						
EHMT1		40						1
EHMT2		25						3
KAT2A		65						8
p300		10						3	-> wir stellten fest, dass die Domänen dieses Proteins nur ca. 10% der Seq.-Länge (falls nicht überlappend) einnehmen
SETD1B		/ (5)					49  (34)
SUV39H1 	35						25


Desweiteren arbeiten wir an übersichtlichen und umfänglichen Darstellungen und Informationssammlungen für die 
betrachteten Spezies und den zugehörigen Stammbaum. 

Wir haben den Output von hmm-search noch einmal angepasst und uns mit esl-sfetch vertraut gemacht, um hits in Proteomen 
suchen zu können. 
Darüberhinaus arbeiten wir an einem Skript für Domain-Profile der Organismen (sprich, um Outputs von hmm-search besser 
beurteilen zu können und sinnvolle Regeln für die Auswahl relevanter Hits zu finden).


# Tag 6 29.06

Die Domain zusammensetzung unserer Proteine wurde noch einmal untersucht um einen besseren Konsens bezüglich der stark
konservierten domains zu finden.
Eine Grafisch aufbereitete Form davon folgt noch.
Die blast Suche mit pro Protein angepassten threshholds ist abgeschlossen und die
Ergebnisse liegen auf prak08.
Die Pipeline für die domain suche mit hmmsearch sowie extraction der hit sequenzen mit esl-sfetch ist abgeschlossen.  
Hier soll morgen in einem weiteren Schritt die Pipeline dahingehend erweitert werden die domain informationen zu
bündeln und Alignments bezüglich der Proteine auf hmmsearch basis zu erstellen. Des weiteren soll ein Intersect von
blast und hmmsearch realisiert werden, sodass ersichtlich wird wo es übereinstimmungen beziehungsweise unterschiede in
den Ergebnissen gibt. Durch die granularität auf domain ebene können somit hoffentlich aussagen darüber getroffen werden 
welche domains für die etwaige divergenz der Ergebnisse verantwortlich sind.

# Tag 7 30.06

Wir haben unsere Durchführungen weitergebrahct, intensiv die Ergebnisse diskutiert und ausgewertet. 
Die Pipeline für die Domain-Informationen wurde soweit erweitert um die Hit-Abschnitte der Sequenzen zu alignen. Außerdem haben wir versucht mit den Ergebnissen von hmmer unsere erste Bäume mit Splitstree graphisch darzustellen.So haben wir gemerkt, dass wir eine bessere Beschriftung brauchen, leider waren die Bäume zu unübersichtlich. Wir arbeiten an die Umbennung, damit wir morgen Vormittag übersichtliche Bäume erstellen und diskutieren konnen. 

